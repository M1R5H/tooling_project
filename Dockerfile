FROM registry.gitlab.com/gitlab-org/terraform-images/stable:latest

ENV PACKER_URL="https://releases.hashicorp.com/packer/1.7.2/packer_1.7.2_linux_amd64.zip"

RUN apk update && apk add curl && apk add ansible 

RUN cd /bin && curl -O "$PACKER_URL" && unzip /bin/packer_1.7.2_linux_amd64.zip -d /bin/



