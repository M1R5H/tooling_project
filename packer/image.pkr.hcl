variable "region" {
  type    = string
  default = "eu-west-2"
}

locals { timestamp = regex_replace(timestamp(), "[- TZ:]", "") }

source "amazon-ebs" "example" {
  ami_name      = "learn-terraform-packer-${local.timestamp}"
  instance_type = "t2.micro"
  region        = var.region
  
    most_recent = true
    owners      = ["099720109477"]
  }
  ssh_username = "ubuntu"
}