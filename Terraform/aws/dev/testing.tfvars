#Generic Access

access_key = ""
secret_key = ""
key_name   = "nginx"

# ec2

region = "eu-west-2"
#ami = "ami-096cb92bb3580c759"
instance_type               = "t2.medium"
associate_public_ip_address = true

# Autoscaling

lc_name          = "nginx_launch_config"
image_id         = "ami-063b629bde6ba2f70"
asg_name_1       = "nginx_autoscaling_group_1"
asg_name_2       = "nginx_autoscaling_group_2"
min_size         = 1
max_size         = 2
desired_capacity = 1

# State file

dynamo_db_tfstate_name = "nginx-tfstate"
state_s3_bucket        = "nginx-tfstate"

#tf_version = "tf-0.15"
#ec2_name = "nginx_terraform"

# Networking

vpc_cidr_block = "11.10.0.0/16"

subnet_cidr_block-public-1 = "11.10.1.0/24"
subnet_cidr_block-public-2 = "11.10.2.0/24"

private_ips-1 = ["11.10.1.128"]
private_ips-2 = ["11.10.2.128"]

route_cidr_block = "0.0.0.0/0"

# Load Balancer
lb_name = "nginx-alb"
lb_port = "80"
lb_target_group_name = "nginx-alb-target-group"
lb_target_port = "18080"

# Generic Tags

tags = {
  "Name"              = "nginx_terraform_2"
  "terraform_version" = "0.15"
}

# Route53

zone_id = "Z00055537JZ1YVKO87UT"