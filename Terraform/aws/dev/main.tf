provider "aws" {
  access_key = var.access_key
  secret_key = var.secret_key
  region     = var.region
}

# resource "aws_dynamodb_table" "terraform_state_lock" {
#   name           = var.dynamo_db_tfstate_name
#   read_capacity  = 5
#   write_capacity = 5
#   hash_key       = "LockID"

#   attribute {
#     name = "LockID"
#     type = "S"
#   }
# }

# resource "aws_s3_bucket" "terraform-state" {
#   bucket = var.state_s3_bucket
#   acl    = "private"

#   versioning {
#     enabled = true
#   }

#   lifecycle {
#     prevent_destroy = false
#   }
# }

 terraform {
  backend "s3" {
    encrypt        = "true"
    bucket         = "nginx-tfstate"
    region         = "eu-west-2"
    dynamodb_table = "nginx-tfstate"
    key            = "nginx/terraform.tfstate"
  }
}

resource "null_resource" "packer" {
  provisioner "local-exec" {
    command = "cd ../../../packer && packer build nginx_load_balancer.json"
  }
}

module "vpc" {
  source                     = "../modules/network"
  subnet_cidr_block-public-1 = var.subnet_cidr_block-public-1
  subnet_cidr_block-public-2 = var.subnet_cidr_block-public-2
  vpc_cidr_block             = var.vpc_cidr_block
  route_cidr_block           = var.route_cidr_block
  tags                       = var.tags
  private_ips-1              = var.private_ips-1
  private_ips-2              = var.private_ips-2
}

# module "ec2" {
#   source                      = "../modules/ec2"
#   instance_type               = var.instance_type
#   key_name                    = var.key_name
#   depends_on                  = [module.vpc, null_resource.packer ] 
#   subnet_id                   = module.vpc.subnet_id
#   vpc_id                      = module.vpc.vpc_id
#   associate_public_ip_address = var.associate_public_ip_address

#   tags = {
#     "Name"  = "nginx_load_balancer"
#     "terraform_version" = "0.15"
#   }

# }

resource "aws_security_group" "allow_ssh_http" {
  name        = "allow_ssh_http_in"
  description = "Allow SSH and HTTP inbound traffic for ASG"
  vpc_id      = module.vpc.vpc_id
  depends_on  = [module.vpc]

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_security_group" "allow-alb-to-ec2" {
  name        = "allow-alb-to-ec2"
  description = "Allow ALB to EC2"
  vpc_id      = module.vpc.vpc_id
  depends_on  = [module.vpc]

  ingress {  # Allow all
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = [ aws_security_group.allow_ssh_http.id ]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}



module "asg-public-1" {
  source                      = "../modules/asg"
  lc_name                     = var.lc_name
  security_groups             = [aws_security_group.allow-alb-to-ec2.id]
  asg_name                    = var.asg_name_1
  image_id                    = var.image_id # Hardcoded AMI for now
  vpc_id                      = module.vpc.vpc_id
  min_size                    = var.min_size
  max_size                    = var.max_size
  desired_capacity            = var.desired_capacity
  vpc_zone_identifier         = [module.vpc.subnet_id_1]
  instance_type               = var.instance_type
  key_name                    = var.key_name
  associate_public_ip_address = var.associate_public_ip_address
  depends_on                  = [null_resource.packer, module.vpc] # Hardcoded AMI for now
  target_group_arns           = module.alb.aws_lb_target_group
  # ami                         = var.ami

  tags = [
    {
      key                 = "Name"
      value               = "nginx_terraform-public-1"
      propagate_at_launch = true
    }
  ]

}

module "asg-public-2" {
  source                      = "../modules/asg"
  lc_name                     = var.lc_name
  asg_name                    = var.asg_name_2
  security_groups             = [aws_security_group.allow-alb-to-ec2.id]
  image_id                    = var.image_id
  vpc_id                      = module.vpc.vpc_id
  min_size                    = var.min_size
  max_size                    = var.max_size
  desired_capacity            = var.desired_capacity
  vpc_zone_identifier         = [module.vpc.subnet_id_2]
  instance_type               = var.instance_type
  key_name                    = var.key_name
  associate_public_ip_address = var.associate_public_ip_address
  target_group_arns           = module.alb.aws_lb_target_group

  # ami                         = var.ami
  depends_on                  = [null_resource.packer, module.vpc]

  tags = [
    {
      key                 = "Name"
      value               = "nginx_terraform-public-2"
      propagate_at_launch = true
    }
  ]

}

module "alb" {
  source               = "../modules/elb"
  lb_name              = var.lb_name
  lb_port              = var.lb_port
  lb_target_group_name = var.lb_target_group_name
  lb_target_port       = var.lb_target_port
  vpc_id               = module.vpc.vpc_id
  sg_group_id          = [aws_security_group.allow_ssh_http.id] # Not perfect, will need changing
  subnet_id            = [module.vpc.subnet_id_1, module.vpc.subnet_id_2]
}


resource "aws_route53_record" "nginx_cname" {
  zone_id = var.zone_id
  name    = "nginx-test"
  type    = "CNAME"
  ttl     = "60"
  records = [ module.alb.aws_lb_dns_name ]

}