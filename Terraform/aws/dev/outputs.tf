# output "public_dns" {
#     value = module.ec2.public_dns
# }

output "subnet_id_1" {
  value = module.vpc.subnet_id_1
}

output "subnet_id_2" {
  value = module.vpc.subnet_id_2
}

output "sg_id" {
  value = aws_security_group.allow_ssh_http.id
}
output "alb_sg_id" {
  value = aws_security_group.allow-alb-to-ec2.id
}