variable "access_key" {}

variable "secret_key" {}

variable "key_name" {}

variable "region" {}

# variable "ami" {}

variable "image_id" {}

#variable "name" {}

variable "instance_type" {}

variable "tags" {
  description = "Array of tags"
}

# variable "security_groups" {
#   description = "Security Group"
# }

variable "subnet_cidr_block-public-1" {
  description = "CIDR Block of Public Subnet 1"
}


variable "subnet_cidr_block-public-2" {
  description = "CIDR Block of Public Subnet 2"
}

variable "vpc_cidr_block" {
  description = "Subnet CIDR Block of VPC"
}

variable "route_cidr_block" {
  description = "Subnet CIDR Block for the route"
}

variable "private_ips-1" {
  description = "Private IPS for ENI 1"
}

variable "private_ips-2" {
  description = "Private IPS for ENI 2"
}

variable "associate_public_ip_address" {
  description = "Associate public IP or not"
}

variable "dynamo_db_tfstate_name" {
  description = "DynamoDB Terraform State"
}

variable "state_s3_bucket" {
  description = "S3 State Bucket"
}

variable "lc_name" {

}

variable "asg_name_1" {

}

variable "asg_name_2" {

}
variable "min_size" {

}

variable "max_size" {

}

variable "desired_capacity" {

}

# variable "vpc_zone_identifier" {

# }

variable "lb_name" {
}

variable "lb_target_group_name" {
  
}

variable "zone_id" {
  description = "ZoneID of the HostedZone"
}

variable "lb_target_port" {

}

variable "lb_port" {

}