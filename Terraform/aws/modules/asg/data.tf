data "aws_ami" "nginx_ami" {
    most_recent = true
    owners      = ["self"]

    filter {
        name = "name"
        values = ["packer-nginx-load-balancer*"]
    }

   # depends_on = [ null_resource.packer ] 
}