variable "lc_name" {

}

variable "image_id" {

}

variable "instance_type" {

}

variable "key_name" {

}

variable "associate_public_ip_address" {

}

variable "tags" {

}

variable "asg_name" {

}

variable "vpc_id" {

}
variable "min_size" {

}

variable "max_size" {

}

variable "desired_capacity" {

}

variable "vpc_zone_identifier" {

}

variable "security_groups" {
  description = "Security Group"
}

variable "target_group_arns" {
  
}