resource "aws_launch_configuration" "nginx_lc" {

  name_prefix = var.lc_name
  image_id                    = data.aws_ami.nginx_ami.id
  #image_id                    = var.image_id
  instance_type               = var.instance_type
  key_name                    = var.key_name
  associate_public_ip_address = var.associate_public_ip_address
  security_groups             = var.security_groups


  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "nginx_asg" {
  name                 = var.asg_name
  launch_configuration = aws_launch_configuration.nginx_lc.name
  min_size             = var.min_size
  max_size             = var.max_size
  desired_capacity     = var.desired_capacity
  vpc_zone_identifier  = var.vpc_zone_identifier
  tags                 = var.tags
  target_group_arns    = [var.target_group_arns]

  lifecycle {
    create_before_destroy = true
  }
}