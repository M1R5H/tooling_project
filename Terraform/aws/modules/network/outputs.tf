output "subnet_id_1" {
  value = aws_subnet.nginx_subnet-public-1.id
}

output "subnet_id_2" {
  value = aws_subnet.nginx_subnet-public-2.id
}

output "vpc_id" {
  value = aws_vpc.nginx_vpc.id
}
