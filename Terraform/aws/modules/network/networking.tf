resource "aws_vpc" "nginx_vpc" {

  cidr_block = var.vpc_cidr_block
  tags       = var.tags

}

resource "aws_internet_gateway" "nginx_igw" {
  vpc_id = aws_vpc.nginx_vpc.id
  tags   = var.tags

}
resource "aws_route_table" "nginx_route_table" {

  vpc_id = aws_vpc.nginx_vpc.id
  tags   = var.tags

  route {
    cidr_block = var.route_cidr_block
    gateway_id = aws_internet_gateway.nginx_igw.id
  }

}

resource "aws_subnet" "nginx_subnet-public-1" {

  vpc_id            = aws_vpc.nginx_vpc.id
  cidr_block        = var.subnet_cidr_block-public-1
  availability_zone = "eu-west-2a"
  tags              = var.tags

}

resource "aws_subnet" "nginx_subnet-public-2" {

  vpc_id            = aws_vpc.nginx_vpc.id
  cidr_block        = var.subnet_cidr_block-public-2
  availability_zone = "eu-west-2b"
  tags              = var.tags

}

resource "aws_route_table_association" "nginx_route_table_assoc-1" {
  subnet_id      = aws_subnet.nginx_subnet-public-1.id
  route_table_id = aws_route_table.nginx_route_table.id
}

resource "aws_route_table_association" "nginx_route_table_assoc-2" {
  subnet_id      = aws_subnet.nginx_subnet-public-2.id
  route_table_id = aws_route_table.nginx_route_table.id
}
resource "aws_network_interface" "nginx_eni-1" {

  subnet_id   = aws_subnet.nginx_subnet-public-1.id
  private_ips = var.private_ips-1
  tags        = var.tags

}

resource "aws_network_interface" "nginx_eni-2" {

  subnet_id   = aws_subnet.nginx_subnet-public-2.id
  private_ips = var.private_ips-2
  tags        = var.tags

}

