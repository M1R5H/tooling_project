variable "vpc_cidr_block" {
  description = "CIDR Block of VPC"
}

variable "subnet_cidr_block-public-1" {
  description = "CIDR Block of Public Subnet 1"
}

variable "subnet_cidr_block-public-2" {
  description = "CIDR Block of Public Subnet 2"
}

variable "route_cidr_block" {
  description = "CIDR Block for Route"
}
variable "tags" {
  description = "VPC Tags"
}

variable "private_ips-1" {
  description = "Private IPS for ENI 1"
}

variable "private_ips-2" {
  description = "Private IPS for ENI 2"
}