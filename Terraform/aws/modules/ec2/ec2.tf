resource "aws_security_group" "allow_ssh_http" {
  name        = "allow_ssh_http_in_ec2"
  description = "Allow SSH and HTTP inbound traffic for EC2"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_instance" "nginx_terraform" {
  ami                         = data.aws_ami.nginx_ami.id
  instance_type               = var.instance_type
  key_name                    = var.key_name
  subnet_id                   = var.subnet_id
  vpc_security_group_ids      = [aws_security_group.allow_ssh_http.id]
  tags                        = var.tags
  associate_public_ip_address = var.associate_public_ip_address
}