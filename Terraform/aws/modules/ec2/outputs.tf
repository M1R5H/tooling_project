output "public_dns" {
  value = aws_instance.nginx_terraform.public_dns
}

output "ami_id" {
  value = data.aws_ami.nginx_ami.id
}