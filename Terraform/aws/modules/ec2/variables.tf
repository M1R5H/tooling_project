variable "key_name" {}

# variable "ami" {}

variable "instance_type" {}

variable "tags" {
  description = "Array of tags"
}

variable "subnet_id" {
  description = "Subnet ID of the EC2 instance"
}

variable "vpc_id" {
  description = "VPC ID"
}

variable "associate_public_ip_address" {
  description = "Associate public IP or not"
}
