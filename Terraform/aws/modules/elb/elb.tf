resource "aws_lb" "nginx_alb" {
  name               = var.lb_name
  internal           = false
  load_balancer_type = "application"
  security_groups    = var.sg_group_id
  subnets            = var.subnet_id

  # enable_deletion_protection = true

  #   access_logs {
  #     bucket  = aws_s3_bucket.lb_logs.bucket
  #     prefix  = "test-lb"
  #     enabled = true
  #   }

  #   tags = {
  #     Environment = "production"
  #   }
}

resource "aws_lb_target_group" "nginx_target_group" {
  name     = var.lb_target_group_name
  port     = var.lb_target_port
  protocol = "HTTP"
  vpc_id   = var.vpc_id
}

resource "aws_lb_listener" "nginx_lb_listener" {
  load_balancer_arn = aws_lb.nginx_alb.arn
  port              = var.lb_port
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.nginx_target_group.arn
  }
}