variable "lb_name" {
}

variable "sg_group_id" {
}

variable "subnet_id" {

}

variable "lb_target_group_name" {
  
}

variable "vpc_id" {
  
}

variable "lb_target_port" {
  description = "18080"
}

variable "lb_port" {
  description = "80"
}