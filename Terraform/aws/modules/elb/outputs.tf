output "aws_lb_target_group" {
    value = aws_lb_target_group.nginx_target_group.arn
}

output "aws_lb_dns_name" {
    value = aws_lb.nginx_alb.dns_name
}