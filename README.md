# Tooling Project

- Tooling project using Nginx, Packer, Terraform, Gitlab_ci, Ansible

TODO

- VPC Endpoint functionality in Terraform
- Further Jinja2
- Tagging etc
- --vault-password-file in packer extra vars
- Maven (Possibly)

### Running the Terraform Code

- Make sure to set the following env variables in your CLI

```
TF_VAR_access_key="<YOUR AWS ACCESS KEY>
TF_VAR_secret_key="<YOUR AWS SECRET KEY>
TF_VAR_key_name="YOUR SSH KEY"
```

`An exception occurred during task execution. To see the full traceback, use -vvv. The error was: psycopg2.errors.UndefinedObject: role "kong" does not exist`
### Flow

- Route53 CNAME > Load Balancer > Nginx Instance in an ASG & LC
